<h1>Post</h1>
<div class="posts">
    <div><?=$data['post']['first_name']?> <?=$data['post']['last_name']?>:</div>
    <div><?=$data['post']['title']?></div>
    <div><?=$data['post']['description']?></div>
    <div class="date"><?=$data['post']['created']?></div>
</div>

<h1>Comments</h1>
<?php foreach($data['comments'] as $comment):?>
    <div class="posts">
        <div><?=$comment['first_name']?> <?=$comment['last_name']?>:</div>
        <div><?=$comment['comment']?></div>
        <div class="date"><?=$comment['created']?></div>
    </div>
<?php endforeach;?>

<?php if (isset($user)):?>
    <h1>Add new comment</h1>
    <div class="add">
        <form action="/comments/<?=$data['post']['id']?>/add" method="POST">
            <div class="red"><?=($data['error'] ?? '')?></div>
            <div>Text:</div>
            <textarea name="comment"><?=($data['commentInput'] ?? '')?></textarea>
            <input type="submit" class="submit" value="Submit">
        </form>
    </div>
<?php endif;?>

<style>
    .posts{
        border-top:7px solid #505559;
        width:970px;
        background: #eee;
        font: 12px arial, sans-serif;
        padding:10px;
    }

    .posts h2 {
        margin-left: 30px;
        float: left;
        width: 197px;
        height: 45px;
        text-indent: -9999px;
    }

    .posts .date {
        text-align:right;
        font-style:italic;
    }



    .add {
        border-top:7px solid #505559;
        width:970px;
        background: #eee;
        font: 12px arial, sans-serif;
        padding:10px;
    }

    .add textarea{
        width: 700px;
        height:150px;
        display:block;
    }

    .add form .submit {
        height: 23px;
        vertical-align: bottom;
        background: #d743ae;
        color: #fff;
        border: 1px solid #ddd;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    .add .red {
        color:red;
    }

</style>
