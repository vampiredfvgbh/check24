<h1>Posts</h1>
<?php foreach($data['posts'] as $post):?>
    <div class="posts">
        <div><?=$post['first_name']?> <?=$post['last_name']?>:</div>
        <div><a href="/comments/<?=$post['id']?>"><?=$post['title']?></a></div>
        <div><?=$post['description']?></div>
        <div class="date"><?=$post['created']?></div>
    </div>
<?php endforeach;?>

<?php if (isset($user)):?>
    <h1>Add new post</h1>
    <div class="add">
        <form action="/posts/add" method="POST">
            <div>Title</div>
            <div>
                <input type="text" name="title" value="<?=($data['titleInput'] ?? '')?>">
            </div>
            <div class="red"><?=($data['error'] ?? '')?></div>
            <div>Text:</div>
            <textarea name="description"><?=($data['descriptionInput'] ?? '')?></textarea>
            <input type="submit" class="submit" value="Submit">
        </form>
    </div>
<?php endif;?>

<style>
    .posts{
        border-top:7px solid #505559;
        width:970px;
        background: #eee;
        font: 12px arial, sans-serif;
        padding:10px;
    }

    .posts h2 {
        margin-left: 30px;
        float: left;
        width: 197px;
        height: 45px;
        text-indent: -9999px;
    }

    .posts .date {
        text-align:right;
        font-style:italic;
    }



    .add {
        border-top:7px solid #505559;
        width:970px;
        background: #eee;
        font: 12px arial, sans-serif;
        padding:10px;
    }

    .add textarea{
        width: 700px;
        height:150px;
        display:block;
    }

    .add form .submit {
        height: 23px;
        vertical-align: bottom;
        background: #d743ae;
        color: #fff;
        border: 1px solid #ddd;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    .add .red {
        color:red;
    }

</style>
