<div id="header">
    <?php if(isset($user)): ?>
        Hello <?=$user['first_name']?> <?=$user['last_name']?>
        <a href="/posts">Wall</a>
        <a href="/my">My posts</a>
        <a href="/logout">Logout</a>
    <?php else:?>
        <a href="/register">Register</a>
        <form id="search" action="/login" method="POST">
            <label>Email <input type="text" name="email" id="s-user" value="<?=($data['emailInput'] ?? '')?>"></label>
            <label class="red"><?=($data['error'] ?? '')?></label>
            <label>Password<input type="password" name="password" id="s-pass" value=""></label>
            <input type="submit" class="submit" value="Submit">
        </form>
    <?php endif;?>
    <h1>Logo</h1>

</div>
<div>
    <?php if (!empty($flash)):?>
        <div class="flash"><?=$flash?></div>
    <?php endif;?>
</div>
<?php if (isset($view)):?>
    <div class="view">
        <?php include($view);?>
    </div>
<?php endif;?>

<style>
    html, body {margin: 0; padding: 0;}

    .flash{
        font-size:24px;
        color:red;
    }
    #header {
        border-top:7px solid #505559;
        margin:0 auto;
        width:970px;
        background: #eee;
        height: 45px;
        padding: 20px 0;
        font: 12px arial, sans-serif;
    }

    #header h1 {
        margin-left: 30px;
        float: left;
        width: 197px;
        height: 45px;
        text-indent: -9999px;
    }

    #header form {
        float: right;
        margin-right: 30px;
        height: 40px;
        padding-top: 8px;
    }

    #header form label {
        display: inline-block;
        margin: 0 2px;
    }

    #header form input {}

    #header form #s-user,
    #header form #s-pass {
        display: block;
        width: 250px;
        border: 1px solid #eee;
        padding: 3px 0 3px 0;
        margin-bottom: -1px;
    }

    #header form .submit {
        height: 23px;
        vertical-align: bottom;
        background: #d743ae;
        color: #fff;
        border: 1px solid #ddd;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #header .red {
        color:red;
    }

    .view {
        margin:15px;
    }
</style>
