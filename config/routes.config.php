<?php

return [
    /* Retrieve list of routes as pattern => [controller class, 'action', Logged state bool] */
    '/' => [\Test\Communication\PostsController::class, 'index'],
    '/my' => [\Test\Communication\PostsController::class, 'my', true],
    '/posts/add' => [\Test\Communication\PostsController::class, 'add', true],
    '/comments/*' => [\Test\Communication\CommentsController::class, 'index'],
    '/comments/*/add' => [\Test\Communication\CommentsController::class, 'add', true],
    '/login' => [\Test\Communication\LoginController::class, 'login', false],
    '/logout' => [\Test\Communication\LoginController::class, 'logout', true],
    '/register' => [\Test\Communication\LoginController::class, 'register', false],
    '/save' => [\Test\Communication\LoginController::class, 'save', false],
];
