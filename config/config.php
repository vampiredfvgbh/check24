<?php

return [
    'db' => [
        'host' => 'localhost',
        'user' => 'root',
        'name' => 'check24',
        'password' => '',
    ],
    'session' => [
        'timeout' => 3600,
        'alias' => 'check24',
    ],
    'passwords' => [
        'salt' => 'admin',
        'password_hash_algo' => PASSWORD_BCRYPT,
    ],
];