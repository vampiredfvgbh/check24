<?php

namespace Test\Query;

use Application\Query\ApplicationQuery;
use PDO;

class CommentsQuery extends ApplicationQuery
{

    /**
     * @param int $postId
     *
     * @return array
     */
    public function getCommentsData(int $postId): array
    {
        $result = $this->adapter->query(
            'SELECT comments.*, users.first_name, users.last_name from comments
            INNER JOIN users on users.id = comments.user_id
            WHERE comments.post_id = '. $postId .' ORDER by comments.created DESC'
        );
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $userId
     * @param int $postId
     * @param array $data
     *
     * @return integer|null
     */
    public function addCommentData(int $userId, int $postId, array $data)
    {
        $data['user_id'] = $userId;
        $data['post_id'] = $postId;
        return $this->adapter->insert('comments', $data);
    }

}
