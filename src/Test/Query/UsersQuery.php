<?php

namespace Test\Query;

use Application\Query\ApplicationQuery;

class UsersQuery extends ApplicationQuery
{

    /**
     * @return array
     */
    public function getUsersData(): array
    {
        return $this->adapter->fetch('users');
    }

}
