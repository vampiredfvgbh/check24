<?php

namespace Test\Query;

use Application\Query\ApplicationQuery;
use PDO;

class PostsQuery extends ApplicationQuery
{

    /**
     * @return array
     */
    public function getPostsData(): array
    {
        $result = $this->adapter->query(
            'SELECT posts.*, users.first_name, users.last_name from posts 
            INNER JOIN users on users.id = posts.user_id ORDER by created DESC'
        );
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $postId
     *
     * @return array|null
     */
    public function getPostData(int $postId)
    {
        $result = $this->adapter->query(
            'SELECT posts.*, users.first_name, users.last_name from posts 
            INNER JOIN users on users.id = posts.user_id WHERE posts.id=' . $postId. ' LIMIT 1 '
        );
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getMyPostsData(int $userId): array
    {
        return $this->adapter->fetch('posts', ['user_id' => $userId]);
    }

    /**
     * @param int $userId
     * @param array $data
     *
     * @return integer|null
     */
    public function addPostData(int $userId, array $data)
    {
        $data['user_id'] = $userId;
        return $this->adapter->insert('posts', $data);
    }

}
