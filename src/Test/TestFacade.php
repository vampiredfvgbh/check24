<?php

namespace Test;

use Application\ApplicationFacade;

/**
 * @method \Test\TestFactory getFactory()
 */
class TestFacade extends ApplicationFacade implements TestFacadeInterface
{

    /**
     * @return array
     */
    public function getPosts(): array
    {
        return $this->getFactory()->createPostsQuery()->getPostsData();
    }

    /**
     * @param int $postId
     *
     * @return array|null
     */
    public function getPost(int $postId)
    {
        return $this->getFactory()->createPostsQuery()->getPostData($postId);
    }

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getMyPosts(int $userId): array
    {
        return $this->getFactory()->createPostsQuery()->getMyPostsData($userId);
    }

    /**
     * @param int $userId
     * @param array $data
     *
     * @return mixed
     */
    public function addPost(int $userId, array $data)
    {
        return $this->getFactory()->createPostsQuery()->addPostData($userId, $data);
    }

    /**
     * @param int $postId
     *
     * @return mixed
     */
    public function getComments(int $postId): array
    {
        return $this->getFactory()->createCommentsQuery()->getCommentsData($postId);
    }

    /**
     * @param int $userId
     * @param int $postId
     * @param array $data
     *
     * @return mixed
     */
    public function addComment(int $userId, int $postId, array $data)
    {
        return $this->getFactory()->createCommentsQuery()->addCommentData($userId, $postId, $data);
    }

}
