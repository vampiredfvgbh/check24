<?php

namespace Test;

use Application\ApplicationFactory;
use Test\Business\Repetition;
use Test\Query\CommentsQuery;
use Test\Query\PostsQuery;
use Test\Query\UsersQuery;

/**
 * @method \Test\TestDependency getDependency()
 */
class TestFactory extends ApplicationFactory
{

    /**
     * @return Repetition
     */
    public function createRepetition(): Repetition
    {
        return new Repetition(
            $this->getDependency()->getUtilsFacade(),
            $this->createUsersQuery()
        );
    }

    /**
     * @return \Test\Query\UsersQuery
     */
    public function createUsersQuery(): UsersQuery
    {
        return new UsersQuery();
    }

    /**
     * @return \Test\Query\PostsQuery
     */
    public function createPostsQuery(): PostsQuery
    {
        return new PostsQuery();
    }

    /**
     * @return \Test\Query\CommentsQuery
     */
    public function createCommentsQuery(): CommentsQuery
    {
        return new CommentsQuery();
    }

}
