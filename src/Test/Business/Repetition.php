<?php

namespace Test\Business;

use Test\Query\UsersQuery;
use Utils\UtilsFacadeInterface;

class Repetition
{

    /**
     * @var \Utils\UtilsFacadeInterface
     */
    protected $utilsFacade;

    /**
     * @var \Test\Query\UsersQuery
     */
    protected $usersQuery;

    public function __construct(
        UtilsFacadeInterface $utilsFacade,
        UsersQuery $usersQuery
    ) {
        $this->utilsFacade = $utilsFacade;
        $this->usersQuery = $usersQuery;
    }

    /**
     * @return string
     */
    public function getRepeatedJson(): string
    {
        $users = $this->usersQuery->getUsersData();
        $json = '';
        foreach($users as $user){
            $json.=$this->utilsFacade->getJson($user);
        }
        return $json;
    }

}