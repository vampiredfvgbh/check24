<?php

namespace Test;

use Application\ApplicationFacadeInterface;

interface TestFacadeInterface extends ApplicationFacadeInterface
{

    /**
     * @return array
     */
    public function getPosts(): array;

    /**
     * @param int $postId
     *
     * @return array|null
     */
    public function getPost(int $postId);

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getMyPosts(int $userId): array;

    /**
     * @param int $userId
     * @param array $data
     *
     * @return mixed
     */
    public function addPost(int $userId, array $data);

    /**
     * @param int $postId
     *
     * @return mixed
     */
    public function getComments(int $postId): array;

    /**
     * @param int $userId
     * @param int $postId
     * @param array $data
     *
     * @return mixed
     */
    public function addComment(int $userId, int $postId, array $data);

}
