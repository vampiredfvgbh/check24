<?php

namespace Test;

use Application\ApplicationDependency;
use Utils\UtilsFacadeInterface;

/**
 * @method \Test\TestDependency getDependency()
 */
class TestDependency extends ApplicationDependency
{
    /**
     * @return \Utils\UtilsFacadeInterface
     */
    public function getUtilsFacade(): UtilsFacadeInterface
    {
        return $this->locator->getByInterface(UtilsFacadeInterface::class);
    }

}
