<?php

namespace Test\Communication;

use Application\Communication\ApplicationController;

/**
 * @method \Test\TestFacadeInterface getFacade()
 */
class LoginController extends ApplicationController
{

    /**
     *  @return void
     */
    public function loginAction(): void
    {
        $this->setTemplate('default');
        $request = $this->getRequest();

        if(isset($request['email'])){
            $this->setData('emailInput', $request['email']);
        }
        if(isset($request['password'])){
            $this->setData('passwordInput', $request['password']);
        }

        if (!isset($request['email']) || !isset($request['password'])) {
            $this->setData('error', 'Input fields are empty');

            return;
        }
        if(!filter_var($request['email'], FILTER_VALIDATE_EMAIL)){
            $this->setData('error', 'Invalid email');

            return;
        }

        if ($this->getFacade()->login($request['email'], $request['password'])){
            $this->redirect('/');
        }

        $this->setData('error', 'Invalid credentials');

    }

    /**
     *  @return void
     */
    public function logoutAction():void
    {
        $this->getFacade()->logout();
        $this->redirect('/');
    }

    /**
     *  @return void
     */
    public function saveAction(): void
    {
        $this->setTemplate('default');
        $request = $this->getRequest();

        if(isset($request['email'])){
            $this->setData('emailInput', $request['email']);
        }
        if(isset($request['password'])){
            $this->setData('passwordInput', $request['password']);
        }
        if(isset($request['first_name'])){
            $this->setData('firstNameInput', $request['first_name']);
        }
        if(isset($request['last_name'])){
            $this->setData('lastNameInput', $request['last_name']);
        }

        if (!isset($request['email']) || !isset($request['first_name']) || !isset($request['last_name']) || !isset($request['password'])) {
            $this->setData('error', 'Input fields are empty');

            return;
        }
        if(!filter_var($request['email'], FILTER_VALIDATE_EMAIL)){
            $this->setData('error', 'Invalid email');

            return;
        }

        if (strlen($request['password']) < 5 || strlen($request['password']) > 30) {
            $this->setData('error', 'Password should be from 5 symbols to 30');

            return;
        }

        if (strlen($request['first_name']) < 5 || strlen($request['first_name']) > 30) {
            $this->setData('error', 'First name should be from 5 symbols to 30');

            return;
        }

        if (strlen($request['last_name']) < 5 || strlen($request['last_name']) > 30) {
            $this->setData('error', 'Last name should be from 5 symbols to 30');

            return;
        }

        if ($this->getFacade()->register($request)){
            $this->redirect('/');
        }

        $this->setData('error', 'Invalid credentials');

    }
}
