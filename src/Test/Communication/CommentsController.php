<?php

namespace Test\Communication;

use Application\Communication\ApplicationController;

/**
 * @method \Test\TestFacadeInterface getFacade()
 */
class CommentsController extends ApplicationController
{

    /**
     *  @return void
     */
    public function indexAction(): void
    {
        $params = $this->getParams();
        $id = $params[0];
        $post = $this->getFacade()->getPost($id);
        if(!$post){
            $this->redirect('/');
        }
        $this->setTemplate('default');
        $this->setView('comments');
        $comments = $this->getFacade()->getComments($id);
        $this->setData('comments', $comments);
        $this->setData('post', $post);
    }

    /**
     *  @return void
     */
    public function addAction(): void
    {
        $params = $this->getParams();
        $id = $params[0];
        $post = $this->getFacade()->getPost($id);
        if(!$post){
            $this->redirect('/');
        }

        $request = $this->getRequest();

        if (!isset($request['comment'])) {
            $this->redirectWithError($id, 'Empty input values');

            return;
        }

        if (strlen($request['comment']) < 5) {
            $this->redirectWithError($id, 'Comment should be from 5 symbols');

            return;
        }

        $user = $this->getAuth()->getUser();
        $result = $this->getFacade()->addComment($user['id'], $post['id'], [
            'comment' => trim($request['comment']),
        ]);

        if ($result) {
            $this->redirect('/comments/' . $post['id']);

            return;
        }

        $this->redirectWithError($post['id'], 'Comment was not created');
    }

    /**
     * @param string $postId
     * @param string $error
     *
     * @return void
     */
    protected function redirectWithError(string $postId, string $error): void
    {
        $this->setFlashError($error);
        $this->redirect('/comments/'.$postId);
    }

}
