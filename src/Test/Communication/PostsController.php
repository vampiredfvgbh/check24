<?php

namespace Test\Communication;

use Application\Communication\ApplicationController;

/**
 * @method \Test\TestFacadeInterface getFacade()
 */
class PostsController extends ApplicationController
{

    /**
     *  @return void
     */
    public function indexAction(): void
    {
        $this->setTemplate('default');
        $this->setView('posts');
        $posts = $this->getFacade()->getPosts();
        $this->setData('posts', $posts);
    }

    /**
     *  @return void
     */
    public function myAction(): void
    {
        $this->setTemplate('default');
        $this->setView('posts');
        $user = $this->getAuth()->getUser();
        $posts = $this->getFacade()->getMyPosts($user['id']);
        foreach ($posts as $key => $post) {
            $post['first_name'] = $user['first_name'];
            $post['last_name'] = $user['last_name'];
            $posts[$key] = $post;
        }
        $this->setData('posts', $posts);
    }

    /**
     *  @return void
     */
    public function addAction(): void
    {
        $request = $this->getRequest();

        if (!isset($request['title']) || !isset($request['description'])) {
            $this->redirectWithError('Empty input values');

            return;
        }

        if (strlen($request['title']) < 5 || strlen($request['title']) > 30) {
            $this->redirectWithError('Title should be from 5 to 30 symbols');

            return;
        }

        if (strlen($request['description']) < 5) {
            $this->redirectWithError('Description should be from 5 symbols');

            return;
        }

        $user = $this->getAuth()->getUser();
        $result = $this->getFacade()->addPost($user['id'], [
            'title' => trim($request['title']),
            'description' => trim($request['description']),
        ]);

        if ($result) {
            $this->redirect('/comments/' . $result);

            return;
        }

        $this->redirectWithError('Post was not created');
    }

    /**
     * @param string $error
     *
     * @return void
     */
    protected function redirectWithError(string $error): void
    {
        $this->setFlashError($error);
        $this->redirect('/posts');
    }

}
