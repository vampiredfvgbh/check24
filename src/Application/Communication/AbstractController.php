<?php

namespace Application\Communication;

use Application\ApplicationFacadeInterface;
use Application\Business\Auth;
use Application\Business\Session;

abstract class AbstractController
{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var \Application\Business\Session
     */
    protected $session;

    /**
     * @var \Application\Business\Auth
     */
    protected $auth;

    /**
     * @var \Application\ApplicationFacadeInterface
     */
    protected $facade;

    /**
     * @param array $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @param \Application\ApplicationFacadeInterface $facade
     */
    public function setFacade(ApplicationFacadeInterface $facade): void
    {
        $this->facade = $facade;
    }

    /**
     * @param \Application\Business\Session $session
     */
    public function setSession(Session $session): void
    {
        $this->session = $session;
    }

    /**
     * @return array
     */
    protected function getRequest(): array
    {
        return $_REQUEST;
    }

    /**
     * @return \Application\Business\Session
     */
    protected function getSession(): Session
    {
        return $this->session;
    }

    /**
     * @param \Application\Business\Auth $auth
     */
    public function setAuth(Auth $auth): void
    {
        $this->auth = $auth;
    }

    /**
     * @return \Application\Business\Auth
     */
    protected function getAuth(): Auth
    {
        return $this->auth;
    }

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return array
     */
    protected function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return \Application\ApplicationFacadeInterface
     */
    protected function getFacade(): ApplicationFacadeInterface
    {
        return $this->facade;
    }

}