<?php

namespace Application\Communication;

use Application\ApplicationFacadeInterface;
use Application\Business\Auth;
use Application\Business\Session;

interface ApplicationControllerInterface
{

    /**
     * @param array $config
     */
    public function setConfig(array $config): void;

    /**
     * @param array $params
     */
    public function setParams(array $params): void;

    /**
     * @param \Application\ApplicationFacadeInterface $facade
     */
    public function setFacade(ApplicationFacadeInterface $facade): void;

    /**
     * @param \Application\Business\Session $session
     */
    public function setSession(Session $session): void;

    /**
     * @param \Application\Business\Auth $auth
     */
    public function setAuth(Auth $auth): void;

    /**
     * @param string $view
     *
     * @return void
     */
    public function setView(string $view): void;

    /**
     * @param string $template
     *
     * @return void
     */
    public function setTemplate(string $template): void;

    /**
     * @return void
     */
    public function clearTemplate(): void;

    /**
     * @param string $key
     * @param mixed $data
     *
     * @return void
     */
    public function setData(string $key, $data): void;

    /**
     * @return void
     */
    public function view(): void;

    /**
     * @param string $path
     *
     * @return void
     */
    public function redirect(string $path): void;

    /**
     * @return bool
     */
    public function isLogged(): bool;

    /**
     * @param string $error
     *
     * @return void
     */
    public function setFlashError(string $error): void;

    /**
     * @return string|null
     */
    public function checkFlashError();

}
