<?php

namespace Application\Communication;

/**
 * @method \Application\ApplicationFacadeInterface getFacade()
 */
class ApplicationController extends AbstractController implements ApplicationControllerInterface
{

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    protected $view;

    /**
     * @var string
     */
    protected $template = null;

    /**
     * @return bool
     */
    public function isLogged(): bool
    {
        return $this->auth->isLogged();
    }

    /**
     * @param string $view
     *
     * @return void
     */
    public function setView(string $view): void
    {
        $this->view = $view;
    }

    /**
     * @param string $template
     *
     * @return void
     */
    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    /**
     * @return void
     */
    public function clearTemplate(): void
    {
        $this->template = null;
    }

    /**
     * @param string $key
     * @param mixed $data
     *
     * @return void
     */
    public function setData(string $key, $data): void
    {
        $this->data[$key] = $data;
    }

    /**
     * @return void
     */
    public function view(): void
    {
        $data = $this->data;
        $path = $this->data['system']['path'];
        $user = $this->getFacade()->getUser();
        $view = $this->view ? 'view/view/'.$this->view.'.php' : null;
        $flash = $this->checkFlashError();
        if($this->template){
            include 'view/template/'.$this->template.'.php';

            return;
        }

        include 'view/view/'.$this->view.'.php';
    }

    /**
     * @param string $path
     *
     * @return void
     */
    public function redirect(string $path): void
    {
        echo '<script>top.location.href = "' . $path . '";</script>';
    }

    /**
     * @param string $error
     *
     * @return void
     */
    public function setFlashError(string $error): void
    {
        $this->getSession()->set('browserError', $error);
    }

    /**
     * @return string|null
     */
    public function checkFlashError()
    {
        $message = $this->getSession()->get('browserError');
        if($message){
            $this->getSession()->remove('browserError');
        }

        return $message;
    }

}
