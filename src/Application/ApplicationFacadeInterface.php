<?php

namespace Application;

interface ApplicationFacadeInterface
{

    /**
     * @param \Application\ApplicationFactoryInterface $factory
     */
    public function setFactory(ApplicationFactoryInterface $factory): void;

    /**
     * @return \Application\ApplicationFactoryInterface
     */
    public function getFactory(): ApplicationFactoryInterface;

    /**
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function login(string $email, string $password): bool;

    /**
     * @param array $data
     *
     * @return bool
     */
    public function register(array $data): bool;

    /**
     * @return void
     */
    public function logout(): void;

    /**
     * @return null|array
     */
    public function getUser();

}
