<?php

namespace Application;

abstract class AbstractFacade
{

    /**
     * @var \Application\ApplicationFactoryInterface
     */
    protected $factory;

    /**
     * @param \Application\ApplicationFactoryInterface $factory
     */
    public function setFactory(ApplicationFactoryInterface $factory): void
    {
        $this->factory = $factory;
    }

    /**
     * @return \Application\ApplicationFactoryInterface
     */
    public function getFactory(): ApplicationFactoryInterface
    {
        return $this->factory;
    }

}