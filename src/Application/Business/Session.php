<?php

namespace Application\Business;

class Session
{

    /**
     * @var array
     */
    public $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $key
     * @params mixed $value
     *
     * @return void
     */
    public function set(string $key, $value): void
    {
        $this->checkAlias();
        $_SESSION[$this->config['session']['alias']][$key] = $value;
    }

    /**
     * @return void
     */
    public function clear(): void
    {
        if (isset($_SESSION[$this->config['session']['alias']])) {
            unset($_SESSION[$this->config['session']['alias']]);
        }
    }

    /**
     * @param string $key
     *
     * @return mixed
     */

    public function get(string $key)
    {
        $this->checkAlias();

        return $_SESSION[$this->config['session']['alias']][$key] ?? null;
    }

    /**
     * @param string $key
     *
     * @return void
     */

    public function remove(string $key): void
    {
        $this->checkAlias();

        if (isset($_SESSION[$this->config['session']['alias']][$key])) {
            unset ($_SESSION[$this->config['session']['alias']][$key]);
        }
    }

    /**
     * @return void
     */

    protected function checkAlias(): void
    {
        if (!isset($_SESSION[$this->config['session']['alias']])) {
            $_SESSION[$this->config['session']['alias']] = [];
        }
    }

}