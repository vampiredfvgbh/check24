<?php

namespace Application\Business;

use Application\ApplicationFacadeInterface;
use Exception;

class Locator
{

    /**
     * @var array
     */
    public $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $namespace
     *
     * @throws \Exception
     *
     * @return \Application\ApplicationFacadeInterface
     */
    public function get(string $namespace): ApplicationFacadeInterface
    {
        return $this->locateFacadeWithDependencies($namespace);
    }

    /**
     * @param string $interface
     *
     * @throws \Exception
     *
     * @return \Application\ApplicationFacadeInterface
     */
    public function getByInterface(string $interface)
    {
        $components = explode('\\', $interface);
        $namespace = $components[0];
        return $this->locateFacadeWithDependencies($namespace);
    }

    /**
     * @param string $namespace
     *
     * @throws \Exception
     *
     * @return \Application\ApplicationFacadeInterface
     */
    protected function locateFacadeWithDependencies(string $namespace)
    {
        $facadeClassName = $namespace.'\\'.$namespace.'Facade';
        if (!class_exists($facadeClassName)) {
            throw new Exception('Class '.$facadeClassName.' does not exist');
        }
        $facade = new $facadeClassName;

        $factoryClassName = $namespace.'\\'.$namespace.'Factory';
        if (!class_exists($factoryClassName)) {
            throw new Exception('Class '.$factoryClassName.' does not exist');
        }
        $factory = new $factoryClassName;

        $dependencyClassName = $namespace.'\\'.$namespace.'Dependency';
        if (!class_exists($dependencyClassName)) {
            throw new Exception('Class '.$dependencyClassName.' does not exist');
        }
        $dependency = new $dependencyClassName;
        /**
         * @var \Application\ApplicationDependencyInterface $dependency
         */
        $dependency->setLocator($this);

        /**
         * @var \Application\ApplicationFactoryInterface $factory
         */
        $factory->setDependency($dependency);
        $factory->setConfig($this->config);
        /**
         * @var \Application\ApplicationFacadeInterface $facade
         */
        $facade->setFactory($factory);

        return $facade;
    }

}