<?php

namespace Application\Business;

class Router
{

    /**
     * @var array
     */
    public $config;

    /**
     * @var array
     */
    public $routes;

    /**
     * @param array $config
     * @param array $routes
     */
    public function __construct(array $config, array $routes)
    {
        $this->config = $config;
        $this->routes = $routes;
    }

    /**
     * @param string $pattern
     * @param string $path
     *
     * @return bool
     */
    protected function comparePath(string $pattern, string $path): bool
    {
        if ($pattern === $path) {
            return true;
        }
        $patternComponents = explode('/', $pattern);
        $pathComponents = explode('/', $path);
        if (count($patternComponents) !== count($pathComponents)) {

            return false;
        }
        foreach ($patternComponents as $key => $value) {
            if ($pathComponents[$key] !== $value && $value !== '*') {

                return false;
            }
        }

        return true;
    }

    /**
     * @param string $pattern
     * @param string $path
     *
     * @return string[]
     */
    protected function extractParams(string $pattern, string $path): array
    {
        $patternComponents = explode('/', $pattern);
        $pathComponents = explode('/', $path);
        $params = [];

        foreach ($patternComponents as $key => $value) {
            if($value === '*'){
                $params[] = $pathComponents[$key];
            }
        }

        return $params;
    }

    /**
     * @param string $path
     *
     * @throws \Exception
     */
    public function resolve(string $path): void
    {
        foreach($this->routes as $pattern => $controllerData) {
            if($this->comparePath($pattern, $path)) {
                $controller = new $controllerData[0];
                $components = explode('\\', $controllerData[0]);
                $namespace = $components[0];
                /**
                 * @var \Application\Communication\ApplicationControllerInterface $controller
                 */
                $action = $controllerData[1].'Action';
                $params = $this->extractParams($pattern, $path);
                $controller->setConfig($this->config);
                $controller->setParams($params);
                $locator = new Locator($this->config);
                $facade = $locator->get($namespace);
                /**
                 * @var \Application\ApplicationFacadeInterface $facade
                 */
                $controller->setFacade($facade);
                $session = new Session($this->config);
                $controller->setSession($session);
                $controller->setAuth(
                    new Auth($this->config, $session)
                );
                if(isset($controllerData[2]) && $controllerData[2] !== $controller->isLogged()){
                    break;
                }

                $controller->setTemplate('default');
                $controller->setView('default');
                $controller->$action();
                $controller->setData('system',[
                   'path' =>  $pattern,
                ]);
                $controller->view();

                return;
            }
        }

        $this->resolve(array_keys($this->routes)[0]);
    }
}