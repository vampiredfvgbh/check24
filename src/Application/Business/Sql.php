<?php

namespace Application\Business;

use Exception;
use PDO;
use PDOStatement;

class Sql
{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var Sql
     */
    static private $instance;

    /**
     * @var PDO
     */
    private $link;

    private function __construct(array $config)
    {
        $this->config = $config;
        $this->link = new PDO('mysql:dbname=' . $config['db']['name'] . ';host=' . $config['db']['host'],
            $config['db']['user'], $config['db']['password']);
    }

    private function __clone()
    {

    }

    /**
     * @param array $config
     */
    public static function init(array $config)
    {
        static::$instance = new static($config);
    }

    /**
     * @return Sql
     */
    public static function getInstance(): Sql
    {
        return static::$instance;
    }

    /**
     * @param string $table
     * @param array $params
     * @param int|null $limit
     *
     * @return string[]
     */
    public function fetch(string $table, array $params = [], $limit = null): array
    {
        $query = "SELECT * FROM " . $table . $this->where($params) . (is_numeric($limit) && $limit ? (' LIMIT ' . (int)$limit) : '');
        $result = $this->query($query);

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $table
     * @param array $params
     *
     * @return array|null
     */
    public function row(string $table, array $params = [])
    {
        $query = "SELECT * FROM " . $table;
        $result = $this->query($query . $this->where($params) . " LIMIT 1");

        return $result->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $table
     * @param array $fields
     *
     * @return integer|null
     */
    public function insert(string $table, array $fields)
    {
        foreach ($fields as $key => $value) {
            $fields[$key] = $this->escape($value);
        }
        $query = "INSERT INTO " . $table . " (`" . implode("`,`", array_keys($fields)) . "`) VALUES (" . implode(",",
                array_values($fields)) . ")";

        $result = $this->query($query);
        $rows = $result->rowCount();

        if ($rows == 1) {
            return $this->link->lastInsertId();
        } else {
            return null;
        }
    }

    /**
     * @param string $table
     * @param array $fields
     * @param array $where
     *
     * @return integer|null
     */
    public function update($table, array $fields, array $where)
    {
        $query = "";
        foreach ($fields as $key => $value) {
            $query .= "`" . $key . "`=" . $this->escape($value) . ",";
        }
        $query = strlen($query) > 1 ? substr($query, 0, strlen($query) - 1) : $query;
        $newWhere = '';
        foreach ($where as $key => $value) {
            if ($newWhere == '') {
                $newWhere .= " WHERE `" . $key . "`=" . $this->escape($value) . " ";
            } else {
                $newWhere .= " AND `" . $key . "`=" . $this->escape($value) . " ";
            }
        }
        $query = "UPDATE " . $table . " SET " . $query . $newWhere;
        $result = $this->query($query);

        return $result->rowCount();
    }

    /**
     * @param array $params
     *
     * @return string
     */
    protected function where(array $params): string
    {
        $where = "";
        foreach ($params as $key => $value) {
            if (strpos($key, '`') !== false) {
                $fkey = $key;
            } else {
                $fkey = "`" . $key . "`";
            }
            if ($where == "") {
                $where .= " WHERE " . $fkey;
            } else {
                $where .= " AND " . $fkey;
            }
            $where .= "=" . $this->escape($value);
        }
        return $where;
    }

    /**
     * @param string $param
     *
     * @return string
     */
    protected function escape($param): string
    {
        return $this->link->quote($param);
    }

    /**
     * @param string $query
     *
     * @throws \Exception
     *
     * @return \PDOStatement
     */
    public function query(string $query): PDOStatement
    {
        $result = $this->link->prepare($query);
        if ($result->execute()) {
            return $result;
        } else {
            throw new Exception("SQL Error! Query [" . $query . "] Error [" . implode(',', $result->errorInfo()) . "]");
        }
    }

}
