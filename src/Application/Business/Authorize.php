<?php

namespace Application\Business;

use Application\Query\AuthQueryInterface;

class Authorize
{

    /**
     * @var array
     */
    public $config;

    /**
     * @var \Application\Business\Auth
     */
    public $auth;

    /**
     * @var \Application\Query\AuthQueryInterface
     */
    public $query;

    /**
     * @param array $config
     * @param \Application\Business\Auth $auth
     * @param \Application\Query\AuthQueryInterface $query
     */
    public function __construct(
        array $config,
        Auth $auth,
        AuthQueryInterface $query
    ) {
        $this->config = $config;
        $this->auth = $auth;
        $this->query = $query;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function login(string $email, string $password): bool
    {
        if ($this->auth->isLogged()){
            return false;
        }

        $user = $this->query->getUserByEmail($email);
        if(!$user){
            return false;
        }

        $passwordHash = $user['password'];
        unset($user['password']);

        return $this->auth->login($password, $passwordHash, $user);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function register(array $data): bool
    {
        /*if ($this->auth->isLogged()){
            return false;
        }

        $user = $this->query->getUserByEmail($email);
        if(!$user){
            return false;
        }

        $passwordHash = $user['password'];
        unset($user['password']);

        return $this->auth->login($password, $passwordHash, $user);
        */
        return false;
    }

    /**
     * @return null|array
     */
    public function getUser()
    {
        if ($this->auth->isLogged()){
            return $this->auth->getUser();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isLogged(): bool
    {
        return $this->auth->isLogged();
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $this->auth->logout();
    }

}
