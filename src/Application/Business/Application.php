<?php

namespace Application\Business;

class Application
{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $routes = [];


    /**
     * @param array $config
     * @param array $routes
     */
    private function __construct(
        array $config,
        array $routes
    ) {
        $this->config = $config;
        $this->routes = $routes;
    }

    /**
     * @param array $config
     * @param array $routes
     *
     * @return Application
     */
    public static function init($config, $routes): Application
    {
        return new Application($config, $routes);
    }

    /**
     * @return void
     */
    public function run(): void
    {
        Sql::init($this->config);
        $router = new Router($this->config, $this->routes);
        $router->resolve($_SERVER['REQUEST_URI']);
    }

}