<?php

namespace Application\Business;

class Auth
{

    /**
     * @var array
     */
    public $config;

    /**
     * @var \Application\Business\Session
     */
    public $session;

    /**
     * @param array $config
     * @param \Application\Business\Session $session
     */
    public function __construct(
        array $config,
        Session $session
    ) {
        $this->config = $config;
        $this->session = $session;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $this->session->clear();
    }

    /**
     * @return bool
     */
    public function isLogged(): bool
    {
        return $this->session->get('user') ? true : false;
    }

    /**
     * @return null|array
     */
    public function getUser()
    {
        return $this->session->get('user');
    }

    /**
     * @param string $password
     * @param string $passwordHash
     * @param string[] $userData
     *
     * @return bool
     */
    public function login(string $password, string $passwordHash, array $userData): bool
    {
        if (password_verify($password . $this->config['passwords']['salt'], $passwordHash)){
            $this->session->set('user', $userData);

            return true;
        }

        return false;
    }

    /**
     * @param string $password
     *
     * @return string
     */
    protected function getPassword(string $password): string
    {
        return password_hash($password . $this->config['passwords']['salt'],
            $this->config['passwords']['password_hash_algo']);
    }

}
