<?php

namespace Application\Query;

interface AuthQueryInterface
{

    /**
     * @param string $email
     *
     * @return null|array
     */
    public function getUserByEmail(string $email);

    /**
     * @param string[] $params
     *
     * @return bool
     */
    public function addUser(array $params): bool;

}
