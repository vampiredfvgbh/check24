<?php

namespace Application\Query;

class AuthQuery extends AbstractQuery implements AuthQueryInterface
{

    /**
     * @param string $email
     *
     * @return null|array
     */
    public function getUserByEmail(string $email)
    {
        return $this->adapter->row('users', ['email' => $email]);
    }

    /**
     * @param string[] $params
     *
     * @return bool
     */
    public function addUser(array $params): bool
    {
        return $this->adapter->insert('users', [
            'email' => $params['email'],
            'password' => $params['password'],
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
        ]);
    }

}
