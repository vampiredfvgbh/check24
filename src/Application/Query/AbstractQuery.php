<?php

namespace Application\Query;

use Application\Business\Sql;

abstract class AbstractQuery
{

    /**
     * @var \Application\Business\Sql
     */
    protected $adapter;

    public function __construct()
    {
        $this->adapter = Sql::getInstance();
    }

}
