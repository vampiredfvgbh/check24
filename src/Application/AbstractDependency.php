<?php

namespace Application;

use Application\Business\Locator;

abstract class AbstractDependency
{

    /**
     * @var \Application\Business\Locator
     */
    protected $locator;

    /**
     * @param \Application\Business\Locator $locator
     */
    public function setLocator(Locator $locator): void
    {
        $this->locator = $locator;
    }

}