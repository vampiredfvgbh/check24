<?php

namespace Application;

/**
 * @method \Application\ApplicationFactoryInterface getFactory()
 */
class ApplicationFacade extends AbstractFacade implements ApplicationFacadeInterface
{

    /**
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function login(string $email, string $password): bool
    {
        return $this->getFactory()->createAuthorizer()->login($email, $password);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function register(array $data): bool
    {
        return $this->getFactory()->createAuthorizer()->register($data);
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $this->getFactory()->createAuthorizer()->logout();
    }

    /**
     * @return null|array
     */
    public function getUser()
    {
        return $this->getFactory()->createAuthorizer()->getUser();
    }
}
