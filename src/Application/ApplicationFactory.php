<?php

namespace Application;

use Application\Business\Auth;
use Application\Business\Authorize;
use Application\Business\Session;
use Application\Query\AuthQuery;
use Application\Query\AuthQueryInterface;

/**
 * @method \Application\ApplicationDependencyInterface getDependency()
 */
class ApplicationFactory extends AbstractFactory implements ApplicationFactoryInterface
{

    /**
     * @return \Application\Query\AuthQueryInterface
     */
    public function createAuthQuery(): AuthQueryInterface
    {
        return new AuthQuery();
    }

    /**
     * @return \Application\Business\Authorize
     */
    public function createAuthorizer(): Authorize
    {
        return new Authorize(
            $this->getConfig(),
            $this->createAuth(),
            $this->createAuthQuery()
        );
    }

    /**
     * @return \Application\Business\Auth
     */
    public function createAuth(): Auth
    {
        return new Auth(
            $this->getConfig(),
            $this->createSession()
        );
    }

    /**
     * @return \Application\Business\Session
     */
    public function createSession(): Session
    {
        return new Session(
            $this->getConfig()
        );
    }
}
