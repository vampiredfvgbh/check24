<?php

namespace Application;

use Application\Business\Auth;
use Application\Business\Authorize;
use Application\Business\Session;
use Application\Query\AuthQueryInterface;

interface ApplicationFactoryInterface
{

    /**
     * @param \Application\ApplicationDependencyInterface $dependency
     */
    public function setDependency(ApplicationDependencyInterface $dependency): void;

    /**
     * @return \Application\ApplicationDependencyInterface
     */
    public function getDependency(): ApplicationDependencyInterface;

    /**
     * @param array $config
     */
    public function setConfig(array $config): void;

    /**
     * @return array
     */
    public function getConfig(): array;

    /**
     * @return \Application\Query\AuthQueryInterface
     */
    public function createAuthQuery(): AuthQueryInterface;

    /**
     * @return \Application\Business\Authorize
     */
    public function createAuthorizer(): Authorize;

    /**
     * @return \Application\Business\Auth
     */
    public function createAuth(): Auth;

    /**
     * @return \Application\Business\Session
     */
    public function createSession(): Session;

}
