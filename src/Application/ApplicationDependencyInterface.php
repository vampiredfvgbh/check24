<?php

namespace Application;

use Application\Business\Locator;

interface ApplicationDependencyInterface
{

    /**
     * @param \Application\Business\Locator $locator
     */
    public function setLocator(Locator $locator): void;

}
