<?php

namespace Application;

abstract class AbstractFactory
{

    /**
     * @var \Application\ApplicationDependencyInterface
     */
    protected $dependency;

    /**
     * @var array
     */
    protected $config;

    /**
     * @param \Application\ApplicationDependencyInterface $dependency
     */
    public function setDependency(ApplicationDependencyInterface $dependency): void
    {
        $this->dependency = $dependency;
    }

    /**
     * @return \Application\ApplicationDependencyInterface
     */
    public function getDependency(): ApplicationDependencyInterface
    {
        return $this->dependency;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

}