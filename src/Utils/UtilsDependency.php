<?php

namespace Utils;

use Application\ApplicationDependency;

/**
 * @method \Utils\UtilsDependency getDependency()
 */
class UtilsDependency extends ApplicationDependency
{
}
