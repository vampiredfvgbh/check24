<?php

namespace Utils\Business;

class JsonUtil
{

    /**
     * @param array $params
     *
     * @return string
     */
    public function jsonEncode(array $params): string
    {
        return json_encode($params);
    }

}