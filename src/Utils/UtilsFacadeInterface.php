<?php

namespace Utils;

use Application\ApplicationFacadeInterface;

interface UtilsFacadeInterface extends ApplicationFacadeInterface
{

    /**
     * @param array $params
     *
     * @return string
     */
    public function getJson(array $params): string;

}
