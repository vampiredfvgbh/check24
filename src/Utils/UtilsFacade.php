<?php

namespace Utils;

use Application\ApplicationFacade;

/**
 * @method \Utils\UtilsFactory getFactory()
 */
class UtilsFacade extends ApplicationFacade implements UtilsFacadeInterface
{
    /**
     * @param array $params
     *
     * @return string
     */
    public function getJson(array $params): string
    {
        return $this->getFactory()->createJsonUtil()->jsonEncode($params);
    }
}
