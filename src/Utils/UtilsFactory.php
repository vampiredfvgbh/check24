<?php

namespace Utils;

use Application\ApplicationFactory;
use Utils\Business\JsonUtil;

/**
 * @method \Test\UtilsDependency getDependency()
 */
class UtilsFactory extends ApplicationFactory
{
    public function createJsonUtil(): JsonUtil
    {
        return new JsonUtil();
    }
}
