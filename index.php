<?php
session_start();
ini_set("max_execution_time","0");
chdir(__DIR__);

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server') {
    $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Composer autoloading
include __DIR__ . '/vendor/autoload.php';



// Retrieve configuration
$appConfig = require __DIR__ . '/config/config.php';
$routesConfig = require __DIR__ . '/config/routes.config.php';
if (file_exists(__DIR__ . '/config/local.config.php')) {
    $appConfig = array_merge($appConfig, require __DIR__ . '/config/local.config.php');
}

spl_autoload_register(function($class) {
    include 'src/' . $class . '.php';
});
// Run the application!
\Application\Business\Application::init($appConfig, $routesConfig)->run();
